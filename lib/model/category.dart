// To parse this JSON data, do
//
//     final cartegory = cartegoryFromJson(jsonString);

import 'dart:convert';
import 'package:get/get.dart';

Cartegory cartegoryFromJson(String str) => Cartegory.fromJson(json.decode(str));

String cartegoryToJson(Cartegory data) => json.encode(data.toJson());

class Cartegory {
  Cartegory({
    this.meals,
  });

  List<ItemCategory> meals;

  factory Cartegory.fromJson(Map<String, dynamic> json) => Cartegory(
    meals: json["meals"] == null ? null : List<ItemCategory>.from(json["meals"].map((x) => ItemCategory.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "meals": meals == null ? null : List<dynamic>.from(meals.map((x) => x.toJson())),
  };
}

class ItemCategory {
  ItemCategory({
    this.strCategory,
    this.isChecked,
  });


  String strCategory;
  bool isChecked;

  factory ItemCategory.fromJson(Map<String, dynamic> json) => ItemCategory(
    strCategory: json["strCategory"] == null ? null : json["strCategory"],
    isChecked: json["is_checked"] == null ? false : json["is_checked"],
  );

  Map<String, dynamic> toJson() => {
    "strCategory": strCategory == null ? null : strCategory,
    "is_checked": isChecked == null ? false : isChecked,
  };
}
