// To parse this JSON data, do
//
//     final mealDetail = mealDetailFromJson(jsonString);

import 'dart:convert';

MealDetail mealDetailFromJson(String str) => MealDetail.fromJson(json.decode(str));

String mealDetailToJson(MealDetail data) => json.encode(data.toJson());

class MealDetail {
  MealDetail({
    this.meals,
  });

  List<Map<String, String>> meals;

  factory MealDetail.fromJson(Map<String, dynamic> json) => MealDetail(
    meals: json["meals"] == null ? null : List<Map<String, String>>.from(json["meals"].map((x) => Map.from(x).map((k, v) => MapEntry<String, String>(k, v == null ? null : v)))),
  );

  Map<String, dynamic> toJson() => {
    "meals": meals == null ? null : List<dynamic>.from(meals.map((x) => Map.from(x).map((k, v) => MapEntry<String, dynamic>(k, v == null ? null : v)))),
  };
}
