


import 'package:flutter_teste_willbank/modules/meal_detail/ui/MeailDetailController.dart';
import 'package:get/get.dart';

import 'modules/home/ui/home_controller.dart';
import 'modules/home/ui/home_page.dart';
import 'modules/meal_detail/ui/MeailDetailPage.dart';


const String routeHome = "/home";
const String routeMeailDetail = "/meailDetail";

List<GetPage> routes = [
  GetPage(
    name: routeHome,
    page: () => HomePage(),
    binding: BindingsBuilder(() => {
      Get.lazyPut<HomePageController>(() => HomePageController()),
    }),
  ),
  GetPage(
    name: routeMeailDetail,
    page: () => MeailDetailPage(),
    binding: BindingsBuilder(() => {
      Get.lazyPut<MeailDetailController>(() => MeailDetailController()),
    }),
  ),
];


