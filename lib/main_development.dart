import 'dart:async';

import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';


import 'modules/app_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();


  await GlobalConfiguration().loadFromPath("assets/config/development.json");
  await GlobalConfiguration().loadFromPath("assets/config/shared.json");

  runZonedGuarded(() {
    runApp(AppWidget());
  }, (error, stackTrace) {
    print('runZonedGuarded: Caught error in my root zone.');
  });


}
