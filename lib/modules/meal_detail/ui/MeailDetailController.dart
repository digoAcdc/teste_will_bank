import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/model/meal_detail.dart';
import 'package:flutter_teste_willbank/model/meals.dart';
import 'package:flutter_teste_willbank/model/resource.dart';
import 'package:flutter_teste_willbank/modules/meal_detail/user_case/iuc_meail_detail.dart';
import 'package:flutter_teste_willbank/modules/meal_detail/user_case/uc_meail_detail.dart';
import 'package:flutter_teste_willbank/modules/shared/util/colors/colors_util.dart';
import 'package:flutter_teste_willbank/modules/shared/util/dialog/dialogs_util.dart';
import 'package:get/get.dart';

class MeailDetailController extends GetxController {
  Meal idMeal = Meal();
  IUCMeailDetail userCase = Get.put(UCMeailDetail());
  final mealDetail = MealDetail().obs;
  final showLoading = false.obs;

  @override
  void onInit() {
    idMeal = Get.arguments;
    getMealDetail();
  }

  getMealDetail() async {
    if (GetUtils.isNumericOnly(idMeal.idMeal)) {
      showLoading.value = true;
      Resource resource = await userCase.findByMealDetailFromId(
          id: int.tryParse(idMeal.idMeal));

      switch (resource.stateEnum) {
        case ResourceStateEnum.SUCCESS:
          MealDetail md = resource.data;
          mealDetail.value = md;
          showLoading.value = false;
          break;
        case ResourceStateEnum.ERROR:
          showLoading.value = false;
          showSnack(
              titulo: "Erro", mensagem: "Falha ao buscar detalhe da refeição");
          break;
        case ResourceStateEnum.NONE:
          // TODO: Handle this case.
          break;
      }
    }
  }

  shareMealDetail() {
    userCase.shareMealDetail(
        url: idMeal.strMealThumb,
        texto: mealDetail.value.meals.first["strInstructions"]);
  }
}
