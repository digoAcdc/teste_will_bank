
import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/model/meals.dart';
import 'package:flutter_teste_willbank/modules/meal_detail/ui/MeailDetailController.dart';
import 'package:get/get.dart';

class MeailDetailPage extends StatefulWidget {
  @override
  _MeailDetailPageState createState() => _MeailDetailPageState();
}

class _MeailDetailPageState extends State<MeailDetailPage> {

  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = new ScrollController();
    _scrollController.addListener(() => setState(() {}));
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  MeailDetailController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    Meal meal = Get.arguments;
    return Scaffold(
      body:  Stack(
        children: <Widget>[
           CustomScrollView(
            controller: _scrollController,
            slivers: [
              new SliverAppBar(
                expandedHeight: 256.0,
                pinned: true,
                flexibleSpace: new FlexibleSpaceBar(
                  title: new Text(meal.strMeal),
                  background: Hero(
                    tag: meal.idMeal,
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/image_placeholder.png",
                      image: meal.strMealThumb,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
               SliverList(
                delegate: new SliverChildListDelegate(
                  new List.generate(
                    1,
                        (int index) => Container(
                          height: Get.height,
                          child: Obx(() {
                            if (controller.mealDetail.value.meals != null)
                              return Container(
                                margin: EdgeInsets.all(16),
                                child: Text(
                                    controller.mealDetail.value.meals.first["strInstructions"]),
                              );
                            else
                              return Container();
                          }),
                        ),
                  ),
                ),
              ),
            ],
          ),
          _buildFab()

        ],
      ),
    );

  }


  Widget _buildFab() {
    //starting fab position
    final double defaultTopMargin = 256.0 - 4.0;
    //pixels from top where scaling should start
    final double scaleStart = 96.0;
    //pixels from top where scaling should end
    final double scaleEnd = scaleStart / 2;

    double top = defaultTopMargin;
    double scale = 1.0;
    if (_scrollController.hasClients) {
      double offset = _scrollController.offset;
      top -= offset;
      if (offset < defaultTopMargin - scaleStart) {
        //offset small => don't scale down
        scale = 1.0;
      } else if (offset < defaultTopMargin - scaleEnd) {
        //offset between scaleStart and scaleEnd => scale down
        scale = (defaultTopMargin - scaleEnd - offset) / scaleEnd;
      } else {
        //offset passed scaleEnd => hide fab
        scale = 0.0;
      }
    }

    return Positioned(
      top: top,
      right: 16.0,
      child: new Transform(
        transform: new Matrix4.identity()..scale(scale),
        alignment: Alignment.center,
        child: FloatingActionButton(
            child: Icon(Icons.share),
            onPressed: () {
              controller.shareMealDetail();
            }),
      ),
    );
  }
}
