import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/model/resource.dart';

abstract class IMeailDetailRepository{
  Future<Resource> findByMealDetailFromId({@required int id});
}