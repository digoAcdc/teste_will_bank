import 'package:dio/dio.dart' as Dio;
import 'package:flutter_teste_willbank/model/meal_detail.dart';
import 'package:flutter_teste_willbank/model/resource.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import 'IMeailDetailRepository.dart';

class MeailDetailRepository implements IMeailDetailRepository{

  Dio.Dio dio = Get.find();

  @override
  Future<Resource> findByMealDetailFromId({int id}) async{
    try{
      final String url = "${GlobalConfiguration().getValue('PATH_MEAL_DETAIL')}";
      Dio.Response response =  await dio.get(url,queryParameters: {
        "i": id,
      });

      Resource r = Resource();

      if (response.statusCode > 199 && response.statusCode < 300) {
        MealDetail mealsDetail = MealDetail.fromJson(response.data);
        r.stateEnum = ResourceStateEnum.SUCCESS;
        r.data = mealsDetail;
        return r;
      } else {
        r.stateEnum = ResourceStateEnum.ERROR;
        r.message = response.data["message"];
        return r;
      }


    }on Dio.DioError catch (e) {
      Resource r = Resource();
      r.stateEnum = ResourceStateEnum.ERROR;
      r.message = e.message;
      return r;
    }
  }

}