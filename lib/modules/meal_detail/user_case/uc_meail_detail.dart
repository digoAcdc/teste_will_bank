import 'dart:io';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_teste_willbank/model/resource.dart';
import 'package:flutter_teste_willbank/modules/meal_detail/repository/IMeailDetailRepository.dart';
import 'package:flutter_teste_willbank/modules/meal_detail/repository/MeailDetailRepository.dart';
import 'package:get/get.dart';
import 'dart:typed_data';
import 'iuc_meail_detail.dart';

class UCMeailDetail implements IUCMeailDetail {
  IMeailDetailRepository repository = Get.put(MeailDetailRepository());

  @override
  Future<Resource> findByMealDetailFromId({int id}) async {
    Resource resource = await repository.findByMealDetailFromId(id: id);
    return resource;
  }

  @override
  Future<Resource> shareMealDetail({String url, String texto}) async{

    try{
      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      Uint8List bytes = await consolidateHttpClientResponseBytes(response);
      await Share.file('Will Bank', 'image.jpg', bytes, 'image/jpg', text: texto);
      Resource resource = Resource();
      resource.stateEnum = ResourceStateEnum.SUCCESS;

      return resource;

    }catch(e){
      Resource resource = Resource();
      resource.stateEnum = ResourceStateEnum.ERROR;

      return resource;
    }


  }
}

