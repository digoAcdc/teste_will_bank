import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/model/resource.dart';

abstract class IUCMeailDetail{
  Future<Resource> findByMealDetailFromId({@required int id});
  Future<Resource> shareMealDetail({@required String url, @required String texto});
}