import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.7),
      width: Get.width,
      height: Get.height,
      child: FlareActor("assets/flare/loading.flr",
          alignment: Alignment.center, fit: BoxFit.contain, animation: "loading"),
    );
  }
}
