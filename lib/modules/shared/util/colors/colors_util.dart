import 'package:flutter/material.dart';

import 'hexa_color.dart';

Map<int, Color> primaryColorCodes = HexColor.getSwatch(HexColor("#f8dd03"));
Map<int, Color> accentColorCodes = HexColor.getSwatch(HexColor("#c8b900"));

MaterialColor primaryColorMaterial = MaterialColor(HexColor.getColorFromHex("#f8dd03"), primaryColorCodes);
MaterialColor accentColorMaterial = MaterialColor(HexColor.getColorFromHex("#c8b900"), accentColorCodes);

Color get getPrimaryColor => primaryColorMaterial;
Color get getAccentColor => accentColorMaterial;

