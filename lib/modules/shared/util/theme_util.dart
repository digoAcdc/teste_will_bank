import 'package:flutter/material.dart';

import 'colors/colors_util.dart';


ThemeData themeData = ThemeData(
  primaryColor: primaryColorMaterial,
  accentColor: accentColorMaterial,
  visualDensity: VisualDensity.adaptivePlatformDensity,

  textTheme: TextTheme(
    headline1: TextStyle(
      fontSize: 42,

      fontWeight: FontWeight.w700,
    ),
    headline2: TextStyle(
      fontSize: 32,

      fontWeight: FontWeight.w700,
    ),
    headline3: TextStyle(
      fontSize: 16,

    ),
    headline4: TextStyle(
      fontSize: 12,

      color: Colors.red,
    ),
    headline5: TextStyle(

      fontSize: 16,

    ),

  ),
);

class ThemeUtil {


  static TextStyle getHeadline1() => themeData.textTheme.headline1;

  static TextStyle getHeadline2() => themeData.textTheme.headline2;

  static TextStyle getHeadline3() => themeData.textTheme.headline3;

  static TextStyle getHeadline4() => themeData.textTheme.headline4;

  static TextStyle getHeadline5() => themeData.textTheme.headline5;

  static ThemeData getThemeData() => themeData;
}
