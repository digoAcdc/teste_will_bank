import 'package:flutter/foundation.dart';
import 'package:flutter_teste_willbank/modules/shared/util/colors/colors_util.dart';
import 'package:get/get.dart';

showSnack({String titulo = "", @required String mensagem}) {
  Get.snackbar(titulo, mensagem,
      snackPosition: SnackPosition.BOTTOM, backgroundColor: getAccentColor);
}
