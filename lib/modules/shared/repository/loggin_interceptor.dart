
import 'package:dio/dio.dart' as Dio;
import 'package:global_configuration/global_configuration.dart';


class LogginInterceptor extends Dio.InterceptorsWrapper {
  bool showDialog = true;

  @override
  Future onRequest(Dio.RequestOptions options) async {
    options.baseUrl = "${GlobalConfiguration().getValue('BASE_URL')}";

    return super.onRequest(options);
  }

  @override
  Future onResponse(Dio.Response response) {

    return super.onResponse(response);
  }

  @override
  Future onError(Dio.DioError error) {
    if (error.type == Dio.DioErrorType.DEFAULT || error.type == Dio.DioErrorType.CONNECT_TIMEOUT) {
      print("ERROR[${error?.error}] => PATH: ${error?.request?.path}");

      return super.onError(error);
    }


    return super.onError(error);
  }

}
