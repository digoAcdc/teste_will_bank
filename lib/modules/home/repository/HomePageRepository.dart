import 'package:dio/dio.dart' as Dio;
import 'package:flutter_teste_willbank/model/category.dart';
import 'package:flutter_teste_willbank/model/meals.dart';

import 'package:flutter_teste_willbank/model/resource.dart';
import 'package:flutter_teste_willbank/modules/shared/repository/loggin_interceptor.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import 'IHomePageRepository.dart';

class HomePageRepository implements IHomePageRepository {
  Dio.Dio dio =
      Get.put(Dio.Dio()..interceptors.add(LogginInterceptor()), permanent: true);

  @override
  Future<Resource> findCategorys() async{

    try{
      final String url = "${GlobalConfiguration().getValue('PATH_CATEGORYS')}";

      Dio.Response response =  await dio.get(url);

      Resource r = Resource();

      if (response.statusCode > 199 && response.statusCode < 300) {
        Cartegory category = Cartegory.fromJson(response.data);
        r.stateEnum = ResourceStateEnum.SUCCESS;
        r.data = category;
        return r;
      } else {
        r.stateEnum = ResourceStateEnum.ERROR;
        r.message = response.data["message"];
        return r;
      }


    }on Dio.DioError catch (e) {
      Resource r = Resource();
      r.stateEnum = ResourceStateEnum.ERROR;
      r.message = e.message;
      return r;
    }
  }

  @override
  Future<Resource> findByMealFromCategory({String category}) async{
    try{
      final String url = "${GlobalConfiguration().getValue('PATH_MEAL_FROM_CATEGORY')}";
      Dio.Response response =  await dio.get(url,queryParameters: {
        "c": category,
      });

      Resource r = Resource();

      if (response.statusCode > 199 && response.statusCode < 300) {
        Meals meals = Meals.fromJson(response.data);
        r.stateEnum = ResourceStateEnum.SUCCESS;
        r.data = meals;
        return r;
      } else {
        r.stateEnum = ResourceStateEnum.ERROR;
        r.message = response.data["message"];
        return r;
      }


    }on Dio.DioError catch (e) {
      Resource r = Resource();
      r.stateEnum = ResourceStateEnum.ERROR;
      r.message = e.message;
      return r;
    }
  }

}
