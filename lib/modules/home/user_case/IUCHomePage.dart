import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/model/resource.dart';

abstract class IUCHomePage{
  Future<Resource> findCategorys();
  Future<Resource> findByMealFromCategory({@required String category});
}