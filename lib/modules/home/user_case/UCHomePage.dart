import 'package:flutter_teste_willbank/model/resource.dart';
import 'package:flutter_teste_willbank/modules/home/repository/HomePageRepository.dart';
import 'package:flutter_teste_willbank/modules/home/repository/IHomePageRepository.dart';
import 'package:flutter_teste_willbank/modules/home/user_case/IUCHomePage.dart';
import 'package:get/get.dart';

class UCHomePage implements IUCHomePage{

  IHomePageRepository repository = Get.put(HomePageRepository());

  @override
  Future<Resource> findCategorys() async{
    Resource resource  = await  repository.findCategorys();

    return resource;
  }

  @override
  Future<Resource> findByMealFromCategory({String category}) async{
    Resource resource  = await  repository.findByMealFromCategory(category: category);

    return resource;
  }



}