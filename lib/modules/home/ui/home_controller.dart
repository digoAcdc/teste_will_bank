import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/model/category.dart';
import 'package:flutter_teste_willbank/model/meals.dart';
import 'package:flutter_teste_willbank/model/resource.dart';
import 'package:flutter_teste_willbank/modules/home/user_case/IUCHomePage.dart';
import 'package:flutter_teste_willbank/modules/home/user_case/UCHomePage.dart';
import 'package:flutter_teste_willbank/modules/shared/util/dialog/dialogs_util.dart';
import 'package:get/get.dart';

class HomePageController extends GetxController {
  @override
  void onInit() {
    refresh();
  }

  final cartegory = Cartegory().obs;
  List<Meal> meals = List<Meal>().obs;
  final loading = true.obs;
  final shimmerCategory = false.obs;
  final shimmerMeal = false.obs;

  IUCHomePage userCase = Get.put(UCHomePage());

  void refresh() {

    cartegory.value.meals = null;
    meals.clear();
    findCategorys();
  }


  findCategorys() async {
    shimmerCategory.value = true;
    Resource resource = await userCase.findCategorys();

    switch (resource.stateEnum) {
      case ResourceStateEnum.SUCCESS:
        cartegory.value = resource.data;
        shimmerCategory.value = false;
        setCategory(0, true);

        break;
      case ResourceStateEnum.ERROR:
        shimmerCategory.value = false;

        showSnack(
            titulo: "Erro", mensagem: "Falha ao Carregar Categorias");

        break;
      case ResourceStateEnum.NONE:
        // TODO: Handle this case.
        break;
    }
  }

  setCategory(int index, bool value) {
    for (var i = 0; i < cartegory.value.meals.length; i++) {
      if (i == index) {
        if (cartegory.value.meals[i].isChecked != true)
          cartegory.value.meals[i].isChecked = value;
      } else {
        cartegory.value.meals[i].isChecked = false;
        findByMealFromCategory(category: cartegory.value.meals[index].strCategory);
      }
    }
  }

  findByMealFromCategory({@required String category}) async {
    shimmerMeal.value = true;
    Resource resource =
        await userCase.findByMealFromCategory(category: category);

    switch (resource.stateEnum) {
      case ResourceStateEnum.SUCCESS:
        Meals m = resource.data;
        meals.clear();
        meals.addAll(m.meals);
        shimmerMeal.value = false;
        break;
      case ResourceStateEnum.ERROR:
        shimmerMeal.value = false;
        showSnack(
            titulo: "Erro", mensagem: "Falha ao Carregar Refeições");

        break;
      case ResourceStateEnum.NONE:
        // TODO: Handle this case.
        break;
    }
  }
}
