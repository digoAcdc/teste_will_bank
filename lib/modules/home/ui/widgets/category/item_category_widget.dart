import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/modules/shared/util/colors/colors_util.dart';

class ItemCategoryWidget extends StatelessWidget {
  final String label;
  final Function(bool) onSelected;
  final bool isSelected;

  const ItemCategoryWidget({Key key, @required this.label,@required this.onSelected,@required this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FilterChip(
      padding: EdgeInsets.only(bottom: 4),
      selected: isSelected,
      showCheckmark: true,
      checkmarkColor: getPrimaryColor,
      shape: StadiumBorder(
          side: BorderSide(
            color: isSelected
                ? getAccentColor
                : Colors.grey,
          )),
      label: Text(
        label,
        style: TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.w600,
          color:
          isSelected
              ? Colors.white
              : Colors.black,
        ),
      ),
      backgroundColor: Colors.white,
      selectedColor: getAccentColor,
      onSelected: onSelected,
    );
  }
}
