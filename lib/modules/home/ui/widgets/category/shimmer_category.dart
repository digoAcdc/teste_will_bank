import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/modules/shared/util/colors/colors_util.dart';
import 'package:shimmer/shimmer.dart';

import 'item_category_widget.dart';

class ShimmerCategory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
        baseColor:Colors.grey[300] ,
        highlightColor: Colors.grey[100] ,
      child: Column(
        children: [
          Text("Categorias"),
          Container(
            height: 50,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: 8,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ItemCategoryWidget(isSelected: false,label: "Aguarde", onSelected: (v){},),
                );
    }
    ),
          ),
        ],
      ),);
  }
}
