import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/modules/home/ui/home_controller.dart';
import 'package:flutter_teste_willbank/modules/home/ui/widgets/category/item_category_widget.dart';


class CategoryWidget extends StatefulWidget {
  final HomePageController controller;

  const CategoryWidget({Key key, @required this.controller}) : super(key: key);

  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<CategoryWidget> {
  HomePageController get controller => widget.controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Text("Categorias"),
          Container(
            height: 50,
            child: ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: 8),
              scrollDirection: Axis.horizontal,
              itemCount: controller.cartegory.value.meals.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.only(left: 8, right: 8),
                  child: ItemCategoryWidget(
                    label: controller.cartegory.value.meals[index].strCategory,
                    isSelected: controller.cartegory.value.meals[index].isChecked,
                    onSelected: (bool value) {
                      controller.setCategory(index, value);
                      setState(() {});
                    },
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
