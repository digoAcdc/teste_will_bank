import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_teste_willbank/modules/home/ui/home_controller.dart';
import 'package:flutter_teste_willbank/modules/shared/util/colors/colors_util.dart';
import 'package:get/get.dart';

import 'item_meal_widget.dart';

class MealsWidget extends StatelessWidget {
  HomePageController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Refeições"),
          Container(
              height: Get.height -260,
              width: Get.width,
              child: StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                itemCount: controller.meals.length,
                itemBuilder: (BuildContext context, int index) => new Container(

                  child: ItemMealWidget(
                    meal: controller.meals[index],
                  ),
                ),
                staggeredTileBuilder: (int index) =>
                    new StaggeredTile.count(2, index.isEven ? 2 : 1),
                mainAxisSpacing: 1.0,
                crossAxisSpacing: 1.0,
              )

              )
        ],
      ),
    );
  }
}
