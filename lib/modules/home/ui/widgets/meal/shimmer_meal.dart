import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_teste_willbank/model/meals.dart';
import 'package:flutter_teste_willbank/modules/shared/util/colors/colors_util.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

import 'item_meal_widget.dart';

class ShimmerMeal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Shimmer.fromColors(
      baseColor: Colors.grey[300] ,
      highlightColor: Colors.grey[100],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Categorias"),
          Container(
            height: Get.height,
            child: StaggeredGridView.countBuilder(
              crossAxisCount: 4,
              itemCount: 20,
              itemBuilder: (BuildContext context, int index) => new Container(

                child: ItemMealWidget(
                  meal: Meal(idMeal: "", strMeal: "", strMealThumb: "",),isLoading: true,
                ),
              ),
              staggeredTileBuilder: (int index) =>
              new StaggeredTile.count(2, index.isEven ? 2 : 1),
              mainAxisSpacing: 1.0,
              crossAxisSpacing: 1.0,
            ),
          ),
        ],
      ),);
  }
}
