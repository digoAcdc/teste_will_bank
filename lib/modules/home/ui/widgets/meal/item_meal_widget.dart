import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/model/meals.dart';
import 'package:flutter_teste_willbank/modules/shared/util/colors/colors_util.dart';
import 'package:flutter_teste_willbank/routes.dart';
import 'package:get/get.dart';

class ItemMealWidget extends StatelessWidget {
  final Meal meal;
  final bool isLoading;

  const ItemMealWidget({Key key, @required this.meal, this.isLoading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: getAccentColor,
      elevation: 3,
      child: Container(
        padding: EdgeInsets.all(8),
        width: 200,
        child: GestureDetector(
          onTap: () {
            Get.toNamed(routeMeailDetail, arguments: meal);
          },
          child: Container(
            width: Get.width,
            height: Get.height,
            child: Card(
              child: Hero(
                tag: meal.idMeal,
                child: isLoading == false
                    ? Stack(
                        children: [
                          FadeInImage.assetNetwork(
                            height: Get.height,
                            width: Get.width,
                            placeholder: "assets/images/image_placeholder.png",
                            image: meal.strMealThumb,
                            fit: BoxFit.cover,
                          ),
                         Align(child:  Container(
                           width: Get.width,
                           color: getAccentColor.withOpacity(0.6),
                           child: Text(
                             meal.strMeal,
                             textAlign: TextAlign.center,
                             maxLines: 1,
                             overflow: TextOverflow.ellipsis,
                             style: TextStyle(
                               color: Colors.black,
                             ),
                           ),
                         ),alignment: Alignment.bottomCenter,)
                        ],
                      )
                    : Container(
                        width: 200,
                        height: 200,
                        child:
                            Image.asset("assets/images/image_placeholder.png")),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
