import 'package:flutter/material.dart';
import 'package:flutter_teste_willbank/modules/home/ui/home_controller.dart';
import 'package:flutter_teste_willbank/modules/home/ui/widgets/category/categorys_widget.dart';
import 'package:flutter_teste_willbank/modules/home/ui/widgets/category/shimmer_category.dart';
import 'package:flutter_teste_willbank/modules/home/ui/widgets/meal/meals_widget.dart';
import 'package:flutter_teste_willbank/modules/home/ui/widgets/meal/shimmer_meal.dart';
import 'package:flutter_teste_willbank/modules/shared/widgets/loading.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomePageController> {
  Future<void> refresh() async {
    controller.refresh();
    debugPrint("refresh");
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refresh,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Will Bank"),
        ),
        body: _body(),
      ),
    );
  }

  _body() {
    return Container(
      height: Get.height,
      child: Stack(
        children: [
          ListView(
            children: [
              Obx(() {
                if (controller.shimmerCategory.value == true)
                  return ShimmerCategory();
                else {
                  if (controller.cartegory.value.meals != null) {
                    return CategoryWidget(
                      controller: controller,
                    );
                    //return Column(children: controller.cartegory.value.meals.map((meal) => new Text(meal.strCategory)).toList());
                  } else {
                    return Container();
                  }
                }
              }),
              Divider(),
              Obx(() {
                if (controller.shimmerMeal.value == true)
                  return ShimmerMeal();
                else {
                  if (controller.meals.isNotEmpty)
                    return MealsWidget();
                  else
                    return Container();
                }
              }),
            ],
          ),

          // Obx(()=> controller.loading.value == true ? Loading(): Container())
        ],
      ),
    );
  }

  _getWidgets() {}
}
