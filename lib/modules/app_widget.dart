
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_teste_willbank/modules/shared/util/theme_util.dart';
import 'package:flutter_teste_willbank/routes.dart';
import 'package:get/get.dart';
import 'package:global_configuration/global_configuration.dart';

import 'shared/util/colors/colors_util.dart';


/// {@category Widget}
class AppWidget extends StatefulWidget {
  @override
  _AppWidgetState createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

    return GetMaterialApp(
      title: '',
      debugShowCheckedModeBanner: GlobalConfiguration().getValue('show_debug_banner'),
      theme: ThemeData(
        primaryColor: Colors.amber,
        accentColor: Colors.amberAccent,
        visualDensity: VisualDensity.adaptivePlatformDensity,

        textTheme: TextTheme(
          headline1: TextStyle(
            fontSize: 42,

            fontWeight: FontWeight.w700,
          ),
          headline2: TextStyle(
            fontSize: 32,

            fontWeight: FontWeight.w700,
          ),
          headline3: TextStyle(
            fontSize: 16,

          ),
          headline4: TextStyle(
            fontSize: 12,

            
          ),
          headline5: TextStyle(

            fontSize: 16,

          ),

        ),
      ),

      initialRoute: routeHome,
      getPages: routes,
    );
  }
}
